#!/usr/bin/env bash
SIFS=$IFS
inp=$1
while IFS= read -r line; do
	systemctl disable --now "$line"
	sleep 1
done < "$inp"
IFS=$SIFS