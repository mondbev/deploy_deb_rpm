# deploy_deb_rpm

Deploy deb rpm servers headless with privileged user

# ########################################
#           Author: Shai
#           Date: 11/02/2021
#
#   Purpose: Detect OS system and init:
#           -Disable unwanted services (Hedaless srv)
#           -Add privileged user name moshe
#           -Copy local ssh key to moshe's authorized keys
#
#   Usage:  At remote server get git and clone
#           https://gitlab.com/mondbev/deploy_deb_rpm
#           Change vars to suite you or use default and init.sh
# ##############################################################