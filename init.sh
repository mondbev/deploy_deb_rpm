#!/usr/bin/env bash

#########################################
#           Author: Shai
#           Date: 11/02/2021
#
#   Purpose: Detect OS system and init:
#           -Disable unwanted services (Hedaless srv)
#           -Add privileged user name moshe
#           -Copy local ssh key to moshe's authorized keys
#
#   Usage:  At remote server get git and clone
#           https://gitlab.com/mondbev/deploy_deb_rpm
#           Change vars to suite you or use default and init
###############################################################

            #
# Default vars declaration #
            #
SRV_USER="moshe" # \\ Server user name
SRV_PASS="gyu" # \\ User pass

isUserRoot(){
## Check if user is root  \\ Thanks to droritzz
    
    if [[ "$UID" == 0 ]]; then
    
        echo # \\ User is root, let's move on to check OS distro
    
    else
        echo "User not root, exiting"
        exit
    fi
}

getDistro(){
## Let's check OS distro

    OS_DIST="$(cat /etc/*-release | grep -E '^NAME=' | cut -d'=' -f2)"

    if [[ -z "$OS_DIST" ]] || [[ ${#OS_DIST} -le 3 ]]; then # // check if var empty or -le 3 is the safe side if we count ""
       
        echo "Can't detect distro, exiting"
        exit

    else
        export OS_DIST="$OS_DIST" # // export OS_DIST e.g os distribution so we can use it in the current shell
    fi
}

createUser(){
## Let's create user based on distro
    if [[ "$OS_DIST" =~ "Debian" ]] || [[ "$OS_DIST" =~ "LMDE" ]]; then

        useradd -m -c "Moshe $OS_DIST" -G sudo,adm -s /bin/bash $SRV_USER 

    elif [[ "$OS_DIST" =~ "CentOS" ]] || [[ "$OS_DIST" =~ "Fedora" ]]; then

        useradd -m -c "Moshe $OS_DIST" -G wheel,adm -s /bin/bash $SRV_USER
    fi

    SALT="Q9"
    encPass=$(perl -e "print crypt(${SRV_PASS},${SALT})") # // hardcode a password , can define on var section or use default
    usermod -p "$encPass" $SRV_USER # // Change user password with usermod
}

createAliases(){
## Create aliases
tee -a /home/$SRV_USER/.bashrc <<EOF
######## Custom ########
alias l=ls
alias la='ls -la'
alias mv='mv -v'
alias cp='cp -v'
alias rm='rm -i'
alias vi=vim
alias cl=clear
alias qwerty='echo AMS, here is your qwerty command'
EOF
}


disableServices(){
## Let's disable services based on distro

    if [[ "$OS_DIST" =~ "Debian" ]]; then

        bash read_lines.sh "deb_units_blacklist" # // local file read_lines.sh accept $1 arg and read lines one after another 

    elif [[ "$OS_DIST" =~ "CentOS" ]] || [[ "$OS_DIST" =~ "Fedora" ]]; then

        bash read_lines.sh "rpm_units_blacklist"

    elif [[ "$OS_DIST" =~ "LMDE" ]]; then

        bash read_lines.sh "lmde_units_blacklist"

    else 
        echo "Not in known distros, exiting"
        exit
    fi
}

installMisc(){
## Install SSH and VIM based on distro
    if [[ "$OS_DIST" =~ "Debian" ]] || [[ "$OS_DIST" =~ "LMDE" ]]; then

        apt install openssh-server vim -y
        systemctl enable --now ssh

    elif [[ "$OS_DIST" =~ "CentOS" ]] || [[ "$OS_DIST" =~ "Fedora" ]]; then

        yum install openssh-server vim -y
        systemctl enable --now sshd
        
    fi

}

sshKeyToAuth(){
## Check for .ssh dir and auth file exists, if not create them. finally prompt for local ssh pub key
    if [[ -d "/home/$SRV_USER/.ssh" ]]; then
    # Dir ssh exists
        if [[ -f  "/home/$SRV_USER/.ssh/authorized_keys" ]]; then
        # Auth file exists
            echo
        else
            # Dir ssh exists but auth file not, create it and give $SRV_USER the permissions
            touch /home/$SRV_USER/.ssh/authorized_keys 
            chmod 0755 /home/$SRV_USER/.ssh/authorized_keys
            chown -R $SRV_USER:$SRV_USER /home/$SRV_USER/.ssh
        fi
    else
        # Dir ssh not exists create it
        mkdir /home/$SRV_USER/.ssh
        # Create auth file and give $SRV_USER the permissions
        touch /home/$SRV_USER/.ssh/authorized_keys
        chmod 0755 /home/$SRV_USER/.ssh/authorized_keys
        chown -R $SRV_USER:$SRV_USER /home/$SRV_USER/.ssh
        
    fi
    ## adding workaround to modify sshd_config file tested on Deb 10 and CentOS 8
    sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
    sed -i 's/AuthorizedKeysFile/#AuthorizedKeysFile/g' /etc/ssh/sshd_config  # // on CentOS 8 this line not commented and vice versa on Debian 10
                                                                              # // this way we are commenting in CentOS and adding another # to commented Debian 10
    echo "AuthorizedKeysFile    .ssh/authorized_keys" >> /etc/ssh/sshd_config # // Now we can append custom command
    waitForSSHpaste # // prompt to paste ssh key to authorized keys
}

waitForSSHpaste(){
## Prompt to get SSH pub key and redirect the content to ssh auth file
    read -r -p $'Waiting for SSH pub key\n>> ' ssh_key

    if [ -n "$ssh_key" ]; then
       echo "$ssh_key" >> /home/$SRV_USER/.ssh/authorized_keys
       exit 0 
    fi
}

main(){
    isUserRoot
    getDistro
    createUser
    createAliases
    disableServices
    installMisc
    sshKeyToAuth
}

###### Main func init ######

main

### Functions index ###
# isUserRoot # // check if user root or exit. sudo is forbidden as AMS requires
# getDistro # // export's OS_DIST 
# createUser # // create privileged user name moshe
# createAliases # // create custom aliases for moshe
# disableServices # // disable unnecessary services
# installMisc # // install SSH and vim
# sshKeyToAuth # // check for .ssh dir and authorized file and init waitForSSHpaste function
# waitForSSHpaste # // prompt for local ssh pub_key and paste content to auth key file
### ------------- ###